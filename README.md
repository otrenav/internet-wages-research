
# Internet wages

- Omar Trejo
- March, 2016

This repository contains R code used in research regarding internet and wages
done by [Omar Trejo](http://links.otrenav.com/website) and David Ramírez. The
main idea is that investment in advanced internet technology increases the
inequality gap between social classes. The research being analyzed is based on

> [Forman, Goldfarb, and Greenstein - The internet and local wages (2012)](https://www.scheller.gatech.edu/directory/faculty/forman/pubs/internet_and_local_wages.pdf)

We replicated the research for Mexico, and found a similar result. However,
instrumental variables are still needed to provide some causality arguments
which are currently missing.
